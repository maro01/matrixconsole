


CExceptionFileSyntax::CExceptionFileSyntax() : CException()
{
	pcEFSMessage = (char*)malloc(sizeof(char) * strlen("CExceptionFileSyntax raised") + 1);
	strcpy_s(pcEFSMessage, sizeof("CExceptionFileSyntax raised"), "CExceptionFileSyntax raised");
}



CExceptionFileSyntax::CExceptionFileSyntax(CExceptionFileSyntax* pEFSArg) : CException(pEFSArg)
{
	pcEFSMessage = pEFSArg->EFSGetMessage();
}

CExceptionFileSyntax::~CExceptionFileSyntax()
{
	free(pcEFSMessage);
}



void CExceptionFileSyntax::EAfficherErreur()
{
	if (EGetCodeErreur() == 1) std::cerr<<"\n\n"<<pcEFSMessage<<"\n\n";
	else std::cerr<<"\n\nCExceptionFileSyntax not raised\n\n";
}

void CExceptionFileSyntax::EFSSetMessage(char* pcMessage)
{
	strcpy_s(pcEFSMessage, sizeof(pcMessage), pcMessage);
}

char * CExceptionFileSyntax::EFSGetMessage()
{
	return nullptr;
}
