


CExceptionDivisionZero::CExceptionDivisionZero() : CException()
{
	pcEDZMessage = (char*)malloc(sizeof(char) * strlen("CExceptionDivisionZero raised") + 1);
	strcpy_s(pcEDZMessage, sizeof("CExceptionDivisionZero raised"), "CExceptionDivisionZero raised");
}

CExceptionDivisionZero::CExceptionDivisionZero(CExceptionDivisionZero* pEDZArg) : CException(pEDZArg)
{
	pcEDZMessage = pEDZArg->EDZGettMessage();
}

CExceptionDivisionZero::~CExceptionDivisionZero()
{
	free(pcEDZMessage);
}

void CExceptionDivisionZero::EAfficherErreur()
{
	if (EGetCodeErreur() == 1) std::cerr<<"\n\n"<<pcEDZMessage<<"\n\n";
	else std::cerr<<"\n\nCExceptionDivisionZero not raised\n\n";
}

void CExceptionDivisionZero::EDZSetMessage(char* pcMessage)
{
	strcpy_s(pcEDZMessage, sizeof(pcMessage), pcMessage);
}

char * CExceptionDivisionZero::EDZGettMessage()
{
	return nullptr;
}
