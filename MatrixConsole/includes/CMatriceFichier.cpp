#include "CMatriceFichier.h"

CMatriceFichier::CMatriceFichier()
{
	pcMFFilePath = NULL;
}

CMatriceFichier::CMatriceFichier(CMatriceFichier& pMFArg)
{
	pcMFFilePath = pMFArg.MFGetFilePath();
}

CMatriceFichier::CMatriceFichier(char* pMFArg)
{
	pcMFFilePath = pMFArg;
}

CMatriceFichier::~CMatriceFichier()
{
	free(pcMFFilePath);
}

void CMatriceFichier::MFSetFilePath(char* pcArg)
{
	pcMFFilePath = pcArg;
}

char* CMatriceFichier::MFGetFilePath()
{
	return pcMFFilePath;
}

template<class MType>
CMatriceAvancee<MType>* CMatriceFichier::MFGetMatriceAvancee()
{
	if (!pcMFFilePath)
	{
		CExceptionFileNotFound EFNFException = new CExceptionFileNotFound();
		throw EFNFException;
	}
	
	std::ifstream fichier(pcMFFilePath); //ouverture du fichier .txt 

	if (fichier)
	{

		char* cLine = new char[2048];
		fichier.getline(cLine, 2048);
		for (int i = 0; cLine[i]; i++) { //lecture de la première ligne et on ignore la casse pour plus de flexibilité
			cLine[i] = tolower(cLine[i]);
		}
		if (!strcmp(cLine, "typematrice=double")) //on verifie que la syntax est bonne
		{
			fichier.getline(cLine, 2048);
			char* cNbLines = new char[2048];
			strcpy_s(cNbLines, sizeof(cLine + 9), cLine + 9); 	//on sépare la deuxieme ligne en 2 parties
			cLine[9] = '\0';									//cline servira a verifier la syntaxe et l'autre partie pour construire l'objet matrice
			for (int i = 0; cLine[i]; i++) {
				cLine[i] = tolower(cLine[i]);
			}

			if (!strcmp(cLine, "nblignes="))
			{
				if (atoi(cNbLines) > 0) {
					fichier.getline(cLine, 2048);
					char* cNbCol = new char[2048]; //idem pour le nombre de colonnes
					strcpy_s(cNbCol, sizeof(cLine + 11), cLine + 11);

					cLine[11] = '\0';
					for (int i = 0; cLine[i]; i++) {
						cLine[i] = tolower(cLine[i]);
					}
					
					if (!strcmp(cLine, "nbcolonnes="))
					{
						if (atoi(cNbCol) > 0) {
							fichier.getline(cLine, 2048);
							for (int i = 0; cLine[i]; i++) {
								cLine[i] = tolower(cLine[i]);
							}

							if (!strcmp(cLine, "matrice=["))
							{
								char* cContext = new char[2048];
								double** mat = (double**)malloc(sizeof(double*) * 3);
								for (size_t i = 0; i < 3; i++)
								{
									mat[i] = (double*)malloc(sizeof(double) * 4);
								}

								for (int nBoucleLigne = 0; nBoucleLigne < atoi(cNbLines); nBoucleLigne++)
								{
									fichier.getline(cLine, 2048);
									for (int nBoucleCol = 0; nBoucleCol < atoi(cNbCol); nBoucleCol++)
									{
										strtok_s(cLine, " ", &cContext); // on contruit la matrice en isolant chaque nombre

										mat[nBoucleLigne][nBoucleCol] = atof(cLine);
										cLine = cContext;
									}
								}
								CMatriceAvancee<double>* matrice = new CMatriceAvancee<double>();
								matrice->MSetmat(mat, atoi(cNbLines), atoi(cNbCol));
								//delete []cLine; //erreur "Invalid address specified to RtlValidateHeap" ?
								//delete []cContext;
								//delete []cNbCol;
								//delete []cNbLines;
								return matrice;
							}
						}
					}

				}
			}

		}
		CExceptionFileSyntax EFSException = new CExceptionFileSyntax();
		throw EFSException;

	}
	else
	{
		CExceptionFileNotFound EFNFException = new CExceptionFileNotFound();
		throw EFNFException;
	}
	return NULL;
}
