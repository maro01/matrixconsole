


CExceptionTypeMatrice::CExceptionTypeMatrice() : CException()
{
	pcETMMessage = (char*)malloc(sizeof(char) * strlen("CExceptionTypeMatrice raised") + 1);
	strcpy_s(pcETMMessage, sizeof("CExceptionTypeMatrice raised"), "CExceptionTypeMatrice raised");
}



CExceptionTypeMatrice::CExceptionTypeMatrice(CExceptionTypeMatrice* pETMArg) : CException(pETMArg)
{
	pcETMMessage = pETMArg->pcETMMessage;
}

CExceptionTypeMatrice::~CExceptionTypeMatrice()
{
	free(pcETMMessage);
}



void CExceptionTypeMatrice::EAfficherErreur()
{
	if (EGetCodeErreur() == 1) std::cerr<<"\n\n"<<pcETMMessage<<"\n\n";
	else std::cerr<<"\n\nCExceptionTypeMatrice not raised\n\n";
}

void CExceptionTypeMatrice::ETMSetMessage(char* pcMessage)
{
	strcpy_s(pcETMMessage, sizeof(pcMessage), pcMessage);
}

char * CExceptionTypeMatrice::ETMGetMessage()
{
	return pcETMMessage;
}
