#ifndef C_EXCEPTION_TYPE_MATRICE_H
#define C_EXCEPTION_TYPE_MATRICE_H

#include "CException.h"

class CExceptionTypeMatrice : public CException
{
private:

	char* pcETMMessage;

public:
	CExceptionTypeMatrice();

	CExceptionTypeMatrice(CExceptionTypeMatrice* pETMArg);

	~CExceptionTypeMatrice();


	void EAfficherErreur();

	void ETMSetMessage(char* pcMessage);

	char* ETMGetMessage();

};
#include "CExceptionTypeMatrice.cpp"
#endif
