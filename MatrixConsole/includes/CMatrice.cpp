
template<typename MType>
CMatrice<MType>::CMatrice() :ptMMat(NULL), nMNblignes(0), nMNbcolonnes(0) {}

template<typename MType>
CMatrice<MType>::CMatrice(CMatrice *pMArg)
{
	MSetmat(pMArg->ptMMat, pMArg->nMNblignes, pMArg->nMNbcolonnes);
}

template<class MType>
CMatrice<MType>::CMatrice(char * pcFilePath)
{
}

template<typename MType>
CMatrice<MType>::~CMatrice()
{
	MType** ptPtr = MGetmat();
	for (unsigned int nBoucle = 0; nBoucle < MGetNblignes(); nBoucle++) free(ptPtr[nBoucle]);
	free(ptPtr);
}

template<typename MType>
unsigned int CMatrice<MType>::MGetNblignes()
{
	return nMNblignes;
}

template<typename MType>
unsigned int CMatrice<MType>::MGetNbcolonnes()
{
	return nMNbcolonnes;
}

template <class MType>
MType** CMatrice<MType>::MGetmat()
{
	return ptMMat;
}

template<typename MType>
void CMatrice<MType>::MSetNblignes(unsigned int nArg)
{
	nMNblignes = nArg;
}

template<typename MType>
void CMatrice<MType>::MSetNbcolonnes(unsigned int nArg)
{
	nMNbcolonnes = nArg;
}

template <class MType>
void CMatrice<MType>::MSetmat(MType** ptArg, unsigned int nNbLignes, unsigned int nNbColomns)
{
	//allocation
	ptMMat = (MType **)malloc(nNbLignes * sizeof(MType*));

	   for (unsigned int nBoucle = 0; nBoucle < nNbLignes; nBoucle++)
		      ptMMat[nBoucle] = (MType *)malloc(nNbColomns * sizeof(MType));

	//initialisation
	for (unsigned int nBoucle1 = 0; nBoucle1 < nNbLignes; ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < nNbColomns; ++nBoucle2)
			ptMMat[nBoucle1][nBoucle2] = 0;


	//affectation de la nouvelle matrice
	for (unsigned int nBoucle1 = 0; nBoucle1 < nNbLignes; ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < nNbColomns; ++nBoucle2)
			ptMMat[nBoucle1][nBoucle2] = ptArg[nBoucle1][nBoucle2];

	MSetNblignes(nNbLignes);
	MSetNbcolonnes(nNbColomns);
}


template<typename MType>
void CMatrice<MType>::MAfficherMatrice()
{
	std::cout << "------------->Taille [" << MGetNblignes() << "][" << MGetNbcolonnes() << "]" << std::endl;
	for (unsigned int nBoucle1 = 0; nBoucle1 < MGetNblignes(); ++nBoucle1)
	{
		for (unsigned int nBoucle2 = 0; nBoucle2 < MGetNbcolonnes(); ++nBoucle2)
		{
			std::cout << ptMMat[nBoucle1][nBoucle2] << "\t";
		}
		std::cout << std::endl;
	}
}
