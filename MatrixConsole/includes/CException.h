#ifndef C_EXCEPTION_H
#define C_EXCEPTION_H

#include <iostream>
#include <fstream>
#include <cstring>

class CException
{
private:

	int eECodeErreur;

public:
	CException();

	CException(CException* pEArg);

	int EGetCodeErreur();

	void EModifierCodeErreur(int eArg);

	virtual void EAfficherErreur() = 0;

};
#include "CException.cpp"
#endif
