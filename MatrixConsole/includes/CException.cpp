

CException::CException() : eECodeErreur(0) {}

CException::CException(CException* pEArg) : eECodeErreur(pEArg->EGetCodeErreur()) {}

int CException::EGetCodeErreur()
{
	return eECodeErreur;
}

void CException::EModifierCodeErreur(int eArg)
{
	eECodeErreur = eArg;
}

