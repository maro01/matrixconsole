#ifndef C_MATRICE_AVANCEE_H
#define C_MATRICE_AVANCEE_H

#include "CMatrice.h"
#include "CExceptionDivisionZero.h"

template<class MType>
class CMatriceAvancee : public CMatrice<MType>
{
public:

	CMatriceAvancee<MType>();

	CMatriceAvancee<MType>(CMatriceAvancee<MType>* pMAArg);

	CMatriceAvancee<MType>(char* pcFilePath);

	CMatriceAvancee<MType>* MATranspose();

	CMatriceAvancee<MType>* operator/(MType MAArg);

	CMatriceAvancee<MType>* operator*(MType MAArg);

	CMatriceAvancee<MType>* operator+(MType MAArg);

	CMatriceAvancee<MType>* operator-(MType MAArg);

	CMatriceAvancee<MType>* operator*(CMatriceAvancee<MType>* tC);

	CMatriceAvancee<MType>* operator+(CMatriceAvancee<MType>* pMAArg);

	CMatriceAvancee<MType>* operator-(CMatriceAvancee<MType>* pMAArg);

};
#include "CMatriceAvancee.cpp"
#endif
