
template<class MType>
CMatriceAvancee<MType>::CMatriceAvancee() : CMatrice<MType>() {}

template<class MType>
CMatriceAvancee<MType>::CMatriceAvancee(CMatriceAvancee<MType>* pMAArg) : CMatrice<MType>(pMAArg) {}

template<class MType>
CMatriceAvancee<MType>::CMatriceAvancee(char* pcFilePath)
{

}

template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::operator/(MType MAArg)
{
	// On cre un objet temporaire pour porter le rsultat 
	CMatriceAvancee<MType>* MAResult = new CMatriceAvancee<MType>();

	// on traite l'exception de division sur 0
	if (MAArg == 0)
	{
		CExceptionDivisionZero* EDZObjet = new CExceptionDivisionZero();
		EDZObjet->EModifierCodeErreur(1);
		throw(EDZObjet);
	}
	// objet de copie 
	CMatriceAvancee<MType>* pMAAAide = new CMatriceAvancee<MType>(this);

	MType** pMAArg = pMAAAide->MGetmat();

	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); nBoucle1++)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); nBoucle2++)
			pMAArg[nBoucle1][nBoucle2] /= MAArg;
	MAResult->MSetmat(pMAArg, this->MGetNblignes(), this->MGetNbcolonnes());

	delete pMAAAide;
	return MAResult;
}

template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::operator*(MType MAArg)
{
	// On cre un objet temporaire pour porter le rsultat 
	CMatriceAvancee<MType>* MAResult = new CMatriceAvancee<MType>();

	// objet de copie 
	CMatriceAvancee<MType>* pMAAAide = new CMatriceAvancee<MType>(this);

	MType** pMAArg = pMAAAide->MGetmat();

	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); nBoucle1++)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); nBoucle2++)
			pMAArg[nBoucle1][nBoucle2] *= MAArg;
	MAResult->MSetmat(pMAArg, this->MGetNblignes(), this->MGetNbcolonnes());

	delete pMAAAide;
	return MAResult;
}

template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::operator+(MType MAArg)
{
	// On cre un objet temporaire pour porter le rsultat 
	CMatriceAvancee<MType>* MAResult = new CMatriceAvancee<MType>();

	// objet de copie 
	CMatriceAvancee<MType>* pMAAAide = new CMatriceAvancee<MType>(this);

	MType** pMAArg = pMAAAide->MGetmat();

	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); nBoucle1++)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); nBoucle2++)
			pMAArg[nBoucle1][nBoucle2] += MAArg;
	MAResult->MSetmat(pMAArg, this->MGetNblignes(), this->MGetNbcolonnes());

	delete pMAAAide;
	return MAResult;
}

template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::operator-(MType MAArg)
{
	// On cre un objet temporaire pour porter le rsultat 
	CMatriceAvancee<MType>* MAResult = new CMatriceAvancee<MType>();

	// objet de copie 
	CMatriceAvancee<MType>* pMAAAide = new CMatriceAvancee<MType>(this);

	MType** pMAArg = pMAAAide->MGetmat();

	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); nBoucle1++)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); nBoucle2++)
			pMAArg[nBoucle1][nBoucle2] -= MAArg;
	MAResult->MSetmat(pMAArg, this->MGetNblignes(), this->MGetNbcolonnes());

	delete pMAAAide;
	return MAResult;
}

template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::MATranspose()
{
	
	//objet qui va contenir le rsultat 
	CMatriceAvancee<MType>* MATranspose = new CMatriceAvancee<MType>();


	// pointeur pointe sur la matrice d'objet courant
	MType** pMAArg = this->MGetmat();

	// allocation de matrice temporaire 
	MType **MTrans = (MType **)malloc(this->MGetNbcolonnes() * sizeof(MType*));
	for (unsigned int nBoucle = 0; nBoucle < this->MGetNbcolonnes(); nBoucle++)
		MTrans[nBoucle] = (MType *)malloc(this->MGetNblignes() * sizeof(MType));

	// matrice transpose
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
	{
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); ++nBoucle2) {
			MTrans[nBoucle2][nBoucle1] = pMAArg[nBoucle1][nBoucle2];
		}
	}
	MATranspose->MSetmat(MTrans,this->MGetNbcolonnes(),this->MGetNblignes());

	free(MTrans);

	return MATranspose;
}

template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::operator*(CMatriceAvancee<MType>* pMAArg)
{
	// Exception : non correspondance des matrices
	if (this->MGetNbcolonnes() != pMAArg->MGetNblignes())
	{
		CExceptionMismatchSizeMatrice* EMSMObjet = new CExceptionMismatchSizeMatrice();

		EMSMObjet->EModifierCodeErreur(1);

		throw(EMSMObjet);
	}

	// 2 pointeurs pointent sur les matrices des objets courants
	MType** MAmat1 = this->MGetmat();
	MType** MAmat2 = pMAArg->MGetmat();

	// pointeur contiendra le resultat du produit
	MType **MAMatResult = (MType **)malloc(this->MGetNblignes() * sizeof(MType*));

	// allocation de la matrice de rsultat
	for (unsigned int nBoucle = 0; nBoucle < this->MGetNblignes(); nBoucle++)
		MAMatResult[nBoucle] = (MType *)malloc(pMAArg->MGetNbcolonnes() * sizeof(MType));
	// initialisation de la matrice de rsultat
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < pMAArg->MGetNbcolonnes(); ++nBoucle2)
			     MAMatResult[nBoucle1][nBoucle2] = 0;
	
	// operation de produit
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < pMAArg->MGetNbcolonnes(); ++nBoucle2)
			for (unsigned int nBoucle3 = 0; nBoucle3 < this->MGetNbcolonnes(); ++nBoucle3)   
				      MAMatResult[nBoucle1][nBoucle2] += MAmat1[nBoucle1][nBoucle3] * MAmat2[nBoucle3][nBoucle2];

	// cration d'objet CMatriceAvancee reprsentant l'objet de rsultat de produit entre 2 objets du mme type
	CMatriceAvancee<MType>* MAProduit = new CMatriceAvancee<MType>();

	//affectation de la matrice de rsultat  un objet de type CMatriceAvancee
	MAProduit->MSetmat(MAMatResult, this->MGetNblignes(), pMAArg->MGetNbcolonnes());

	delete MAMatResult;
	
	return MAProduit;
}

template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::operator+(CMatriceAvancee* pMAArg)
{
	// Exception : non correspondance  entre les 2 matrices
	if ( ( this->MGetNblignes() != pMAArg->MGetNblignes() ) || ( this->MGetNbcolonnes() != pMAArg->MGetNbcolonnes() ) )
	{
		CExceptionMismatchSizeMatrice* EMSMObjet = new CExceptionMismatchSizeMatrice();

		EMSMObjet->EModifierCodeErreur(1);

		throw(EMSMObjet);
	}

	// 2 pointeurs pointent sur les matrices des objets courants
	MType** MAmat1 = this->MGetmat();
	MType** MAmat2 = pMAArg->MGetmat();


	// pointeur contiendra le resultat de la somme
	MType **MAMatResult = (MType **)malloc(this->MGetNblignes() * sizeof(MType*));

	// allocation de la matrice de rsultat
	for (unsigned int nBoucle = 0; nBoucle < this->MGetNblignes(); nBoucle++)
		MAMatResult[nBoucle] = (MType *)malloc(this->MGetNbcolonnes() * sizeof(MType));

	// initialisation de la matrice de rsultat
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); ++nBoucle2)
			MAMatResult[nBoucle1][nBoucle2] = 0;

	// operation 
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); ++nBoucle2)
				// operation de division et affectation des valeurs
				MAMatResult[nBoucle1][nBoucle2] = MAmat1[nBoucle1][nBoucle2] + MAmat2[nBoucle1][nBoucle2];

	// cration d'objet CMatriceAvancee reprsentant l'objet de rsultat de produit entre 2 objets du mme type
	CMatriceAvancee<MType>* MASomme = new CMatriceAvancee<MType>();

	//affectation de la matrice de rsultat  un objet de type CMatriceAvancee
	MASomme->MSetmat(MAMatResult, this->MGetNblignes(), pMAArg->MGetNbcolonnes());

	delete MAMatResult;

	return MASomme;
}

template<class MType>
CMatriceAvancee<MType>* CMatriceAvancee<MType>::operator-(CMatriceAvancee* pMAArg)
{
	// Exception : non correspondance  entre les 2 matrices
	if ((this->MGetNblignes() != pMAArg->MGetNblignes()) || (this->MGetNbcolonnes() != pMAArg->MGetNbcolonnes()))
	{
		CExceptionMismatchSizeMatrice* EMSMObjet = new CExceptionMismatchSizeMatrice();

		EMSMObjet->EModifierCodeErreur(1);

		throw(EMSMObjet);
	}

	// 2 pointeurs pointent sur les matrices des objets courants
	MType** MAmat1 = this->MGetmat();
	MType** MAmat2 = pMAArg->MGetmat();


	// pointeur contiendra le resultat de la somme
	MType **MAMatResult = (MType **)malloc(this->MGetNblignes() * sizeof(MType*));

	// allocation de la matrice de rsultat
	for (unsigned int nBoucle = 0; nBoucle < this->MGetNblignes(); nBoucle++)
		MAMatResult[nBoucle] = (MType *)malloc(this->MGetNbcolonnes() * sizeof(MType));

	// initialisation de la matrice de rsultat
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); ++nBoucle2)
			MAMatResult[nBoucle1][nBoucle2] = 0;

	// operation 
	for (unsigned int nBoucle1 = 0; nBoucle1 < this->MGetNblignes(); ++nBoucle1)
		for (unsigned int nBoucle2 = 0; nBoucle2 < this->MGetNbcolonnes(); ++nBoucle2)
			// operation de division et affectation des valeurs
			MAMatResult[nBoucle1][nBoucle2] = MAmat1[nBoucle1][nBoucle2] + MAmat2[nBoucle1][nBoucle2];

	// cration d'objet CMatriceAvancee reprsentant l'objet de rsultat de soustraction entre 2 objets du mme type
	CMatriceAvancee<MType>* MASomme = new CMatriceAvancee<MType>();

	//affectation de la matrice de rsultat  un objet de type CMatriceAvancee
	MASomme->MSetmat(MAMatResult, this->MGetNblignes(), pMAArg->MGetNbcolonnes());

	delete MAMatResult;

	return MASomme;
}
