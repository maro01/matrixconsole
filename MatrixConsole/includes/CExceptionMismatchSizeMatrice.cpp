


CExceptionMismatchSizeMatrice::CExceptionMismatchSizeMatrice()
{
	pcEMSMMessage = (char*)malloc(sizeof(char) * strlen("CExceptionMismatchSizeMatrice raised") + 1);
	strcpy_s(pcEMSMMessage, sizeof("CExceptionMismatchSizeMatrice raised"), "CExceptionMismatchSizeMatrice raised");
}

CExceptionMismatchSizeMatrice::CExceptionMismatchSizeMatrice(CExceptionMismatchSizeMatrice * pEMSMArg)
{
	pcEMSMMessage = pEMSMArg->EMSGetMessage();
}

CExceptionMismatchSizeMatrice::~CExceptionMismatchSizeMatrice()
{
	free(pcEMSMMessage);
}

void CExceptionMismatchSizeMatrice::EAfficherErreur()
{
	if (EGetCodeErreur() == 1) std::cerr << "\n\n" << pcEMSMMessage << "\n\n";
	else std::cerr << "\n\nCExceptionMismatchSizeMatrice not raised\n\n";
}

void CExceptionMismatchSizeMatrice::EMSMSetMessage(char * pcMessage)
{
	strcpy_s(pcEMSMMessage, sizeof(pcMessage), pcMessage);
}

char * CExceptionMismatchSizeMatrice::EMSGetMessage()
{
	return pcEMSMMessage;
}
