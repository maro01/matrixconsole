

CExceptionFileNotFound::CExceptionFileNotFound() : CException()
{
	pcEFNFMessage = (char*)malloc(sizeof(char) * strlen("CExceptionFileNotFound raised") + 1);
	strcpy_s(pcEFNFMessage, sizeof("CExceptionFileNotFound raised"), pcEFNFMessage);
}



CExceptionFileNotFound::CExceptionFileNotFound(CExceptionFileNotFound* pEFNFArg) : CException(pEFNFArg)
{
	pcEFNFMessage = pEFNFArg->EFNFGetMessage();
}

CExceptionFileNotFound::~CExceptionFileNotFound()
{
	free(pcEFNFMessage);
}



void CExceptionFileNotFound::EAfficherErreur()
{
	if (EGetCodeErreur() == 1) std::cerr<<"\n\n"<<pcEFNFMessage<<"\n\n";
	else std::cerr<<"\n\nCExceptionFileNotFound not raised\n\n";
}

void CExceptionFileNotFound::EFNFSetMessage(char* pcMessage)
{
	strcpy_s(pcEFNFMessage, sizeof(pcMessage), pcMessage);
}

char * CExceptionFileNotFound::EFNFGetMessage()
{
	return pcEFNFMessage;
}
