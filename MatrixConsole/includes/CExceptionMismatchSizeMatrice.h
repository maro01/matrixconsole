#ifndef C_EXCEPTION_MISMATCH_SIZE_MATRICE_H
#define C_EXCEPTION_MISMATCH_SIZE_MATRICE_H

#include "CException.h"

class CExceptionMismatchSizeMatrice : public CException
{
private:

	char* pcEMSMMessage;

public:
	CExceptionMismatchSizeMatrice();

	CExceptionMismatchSizeMatrice(CExceptionMismatchSizeMatrice* pEMSMArg);

	~CExceptionMismatchSizeMatrice();

	void EAfficherErreur();

	void EMSMSetMessage(char* pcMessage);

	char* EMSGetMessage();

};
#include "CExceptionMismatchSizeMatrice.cpp"
#endif
